require 'rails_helper'

describe User do 

	before do
		@user = User.new(email: "user@example.com", password: "12345678",  password_confirmation: "12345678")
	end

  it { should respond_to(:email) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }

  describe "when email is not present" do
  	before { @user.email = "" }
  	it {should_not be_valid }
  end

  describe "when email is invalid" do
  	it "should be invalid" do
  		mails = %w[user@example,com user.com user@example.user@example.com user@example..com]

  		mails.each do |invalid_email|
  			@user.email = invalid_email
  			expect(@user).not_to be_valid
  		end
  	end
  end

  describe "when email is valid" do
  	it "should be valid" do
  		mails = %w[user@example.com user_Example@abc.com.mk user_x@example.user name+surname@example.com]

  		mails.each do |invalid_email|
  			@user.email = invalid_email
  			expect(@user).to be_valid
  		end
  	end
  end

  describe "when email address is already taken" do
  	before do
  		second_user = @user.dup
  		second_user.save
  	end

  	it { should_not be_valid }
  end

  describe "when user is created" do
    it "gets a uid assigned" do
      @user.save!
      expect(@user.uid).not_to be_blank
    end
  end
end