require 'rails_helper'

describe Item do 

	before do
		@user = User.new(email: "user@example.com")
		@list = List.new(description: "Some list description", user_id: @user.id)
		@item = Item.new(title: "First Item", list_id: @list.id, state: 0)
	end

  it { should respond_to(:title) }
  it { should respond_to(:list_id) }
  it { should respond_to(:state) }

  describe "when list is not present" do
  	before { @item.list = nil }
  	it {should_not be_valid }
  end

  it "is not valid when state is not correct" do
  	expect { @item.state = 3 }.to raise_error(ArgumentError).with_message(/is not a valid state/)
  end
end