require 'rails_helper'

describe List do 

	before do
		@user = User.new(email: "user@example.com")
		@list = List.new(description: "Some list description", user_id: @user.id)
	end

  it { should respond_to(:description) }
  it { should respond_to(:user_id) }

  describe "when user is not present" do
  	before { @list.user = nil }
  	it {should_not be_valid }
  end
end