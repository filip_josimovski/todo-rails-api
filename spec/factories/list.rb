FactoryGirl.define do
  factory :list do
    description { Faker::Lorem.sentence(3) }
    user
  end
end