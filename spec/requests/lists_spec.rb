require 'rails_helper'

RSpec.describe 'Lists API', type: :request do
  let!(:lists) { create_list(:list, 5) }
  let(:list_id) { lists.first.id }

  describe 'GET /lists' do
    before { get '/api/v1/lists' }

    it 'returns lists' do
      expect(json).not_to be_empty
      expect(json.size).to eq(5)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /lists/:id' do
    before { get "/api/v1/lists/#{list_id}" }

    context 'when the list exists' do
      it 'returns the list' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(list_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the list does not exist' do
      let(:list_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find List/)
      end
    end
  end

  describe 'POST /lists' do
    let(:valid_attributes) { { description: 'This is my first list', user_id: FactoryGirl.create(:user).id } }

    context 'when the request is valid' do
      before { post '/api/v1/lists', params: valid_attributes }

      it 'creates a list' do
        expect(json['description']).to eq('This is my first list')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/api/v1/lists', params: { description: 'This is my first list', user_id: nil } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body).to match(/Validation failed: User must exist/)
      end
    end
  end

  describe 'PUT /lists/:id' do
    let(:valid_attributes) { { title: 'Shopping' } }

    context 'when the record exists' do
      before { put "/api/v1/lists/#{list_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'DELETE /lists/:id' do
    before { delete "/api/v1/lists/#{list_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end