require 'rails_helper'

RSpec.describe 'Authentication API', type: :request do
  describe 'POST /auth signup' do
    it 'returns status 200' do
      post api_v1_user_registration_path(:email => 'email@email.com', :password => '12345678')
      expect(response).to have_http_status(200)
    end

    it 'Should respond with status 200(OK)' do
      expect{
        post api_v1_user_registration_path(:email => 'email@email.com', :password => '12345678')
      }.to change(User, :count).by(1)
    end
  end

  describe 'POST /auth/sign_in signin' do
    it 'Should respond with status 200(OK)' do
      post api_v1_user_registration_path(:email => 'email@email.com', :password => '12345678')
      user = User.last
      post api_v1_user_session_path(:email => 'email@email.com', :password => '12345678')
      expect(response).to be_success
    end
  end
end