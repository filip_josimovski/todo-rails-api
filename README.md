# README

Short readme about setting up the app.

Things you may want to cover:

* Ruby version
2.3.1

* Rails Version
5.0.6

* Instalation 
1. Clone the rails and frontend apps
git clone git@bitbucket.org:filip_josimovski/todo-rails-api.git
git clone git@bitbucket.org:filip_josimovski/todo-frontend.git
2. Create database

* Database creation
bundle exec rake db:create

3. Migrate database
bundle exec rake db:migrate

4. Start the server
rails server

5. Start the frontend with server

* How to run the test suite
bundle exec rspec spec/

