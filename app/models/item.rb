class Item < ApplicationRecord
  belongs_to :list
	enum state: { undone: 0, finished: 1 }
end
