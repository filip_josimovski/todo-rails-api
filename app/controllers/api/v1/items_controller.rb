class Api::V1::ItemsController < Api::BaseController
  before_action :set_item, only: [:show, :update, :destroy]

  # GET /items
  def index
    @items = Item.all
    @items = Item.where(list_id: params[:list_id]).all if params[:list_id]
    json_response(@items)
  end

  # POST /items
  def create
    @item = Item.create!(item_params)
    json_response(@item, :created)
  end

  # GET /items/:id
  def show
    json_response(@item)
  end

  # PUT /items/:id
  def update
    @item.update(item_params)
    head :no_content
  end

  # DELETE /items/:id
  def destroy
    @item.destroy
    head :no_content
  end

  private

  def item_params
    params.permit(:title, :state, :list_id)
  end

  def set_item
    @item = Item.find(params[:id])
  end
end